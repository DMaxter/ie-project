package pt.tecnico.avaas.user;

import org.eclipse.microprofile.reactive.messaging.Incoming;

public class User {
  @Incoming("users")
  public void getActions(String actions) {
    System.out.println("Received actions: " + actions);
  }
}
