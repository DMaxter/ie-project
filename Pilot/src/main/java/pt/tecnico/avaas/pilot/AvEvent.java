package pt.tecnico.avaas.pilot;

public class AvEvent {
  private String timestamp;
  private String AVID;
  private Integer speed;
  private Integer batteryLevel;
  private Integer driverTirenessLevel;
  private String location;
  private Lighting environmentalLighting;
  private Rain rainConditions;
  private Fog fogConditions;
  private Integer tractionWheelsLevel;
  private Integer userId;
  private Integer apilotId;
  private Integer carId;

  public AvEvent() {
  }

  public AvEvent(String timestamp, String id, Integer speed, Integer batteryLevel, Integer driverTirenessLevel,
      String location, Lighting environmentalLighting, Rain rainConditions, Fog fogConditions,
      Integer tractionWheelsLevel) {
    this.timestamp = timestamp;
    this.AVID = id;
    this.speed = speed;
    this.batteryLevel = batteryLevel;
    this.driverTirenessLevel = driverTirenessLevel;
    this.location = location;
    this.environmentalLighting = environmentalLighting;
    this.rainConditions = rainConditions;
    this.fogConditions = fogConditions;
    this.tractionWheelsLevel = tractionWheelsLevel;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getAVID() {
    return AVID;
  }

  public Integer getSpeed() {
    return speed;
  }

  public Integer getBatteryLevel() {
    return batteryLevel;
  }

  public Integer getDriverTirenessLevel() {
    return driverTirenessLevel;
  }

  public String getLocation() {
    return location;
  }

  public Lighting getEnvironmentalLighting() {
    return environmentalLighting;
  }

  public Rain getRainConditions() {
    return rainConditions;
  }

  public Fog getFogConditions() {
    return fogConditions;
  }

  public Integer getTractionWheelsLevel() {
    return tractionWheelsLevel;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Integer getApilotId() {
    return apilotId;
  }

  public void setApilotId(Integer apilotId) {
    this.apilotId = apilotId;
  }

  public Integer getCarId() {
    return carId;
  }

  public void setCarId(Integer carId) {
    this.carId = carId;
  }

  public enum Lighting {
    NA("N/A"),
    BAD("Bad"),
    SUFFICIENT("Sufficient"),
    GOOD("Good"),
    VERY_GOOD("Very Good"),
    EXCELENT("Excelent");

    private final String text;

    Lighting(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  };

  public enum Rain {
    NA("N/A"),
    LIGHT("Light Rain"),
    MEDIUM("Medium Rain"),
    HEAVY("Heavy Rain");

    private final String text;

    Rain(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Fog {
    NA("N/A"),
    NONE("None"),
    LIGHT("Light Fog"),
    MEDIUM("Medium Fog"),
    DENSE("Dense Fog");

    private final String text;

    Fog(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }
}
