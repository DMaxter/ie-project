package pt.tecnico.avaas.pilot;

import java.util.Date;
import java.util.Random;

import javax.enterprise.context.ApplicationScoped;

import org.jboss.logging.Logger;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import pt.tecnico.avaas.pilot.AvResult.Security;
import pt.tecnico.avaas.pilot.AvResult.Utilitarianism;
import pt.tecnico.avaas.pilot.AvEvent.Fog;
import pt.tecnico.avaas.pilot.AvEvent.Lighting;
import pt.tecnico.avaas.pilot.AvEvent.Rain;
import pt.tecnico.avaas.pilot.AvResult.Detection;
import pt.tecnico.avaas.pilot.AvResult.Execution;
import pt.tecnico.avaas.pilot.AvResult.Identification;
import pt.tecnico.avaas.pilot.AvResult.Infrastructure;
import pt.tecnico.avaas.pilot.AvResult.Legitimacy;
import pt.tecnico.avaas.pilot.AvResult.Reaction;
import pt.tecnico.avaas.pilot.AvResult.Responsibility;
import pt.tecnico.avaas.pilot.AvResult.Risk;
import pt.tecnico.avaas.pilot.AvResult.Road;;

@ApplicationScoped
public class Pilot {
  private static final Random RANDOM = new Random();

  private static final Logger LOGGER = Logger.getLogger(Pilot.class);

  @Incoming("pilot")
  @Outgoing("avresults")
  public AvResult convert(AvEvent event) {
    LOGGER.info("Received message from car " + event.getCarId());

    AvResult result = new AvResult();

    result.setApilotId(event.getApilotId());
    result.setCarId(event.getCarId());
    result.setUserId(event.getUserId());
    result.setTimestamp(new Date());

    result.setICTInfrastructure(randomEnum(Infrastructure.class));
    result.setRoadConditions(randomEnum(Road.class));
    result.setLegitimacy(randomEnum(Legitimacy.class));

    generateSecurity(result);
    generateUtilitarianism(event, result);
    generateSocialResponsibility(result);
    generateDetection(event, result);
    generateIdentification(event, result);
    generateRiskAnalysis(event, result);
    generateReaction(event, result);
    generateExecution(result);

    LOGGER.info("Sent message to AVResult topic");

    return result;
  }

  public static void generateExecution(AvResult result) {
    Reaction reaction = result.getReaction();
    Security security = result.getSecurity();

    if (reaction == Reaction.LOW) {
      result.setExecution(Execution.LOW);
    } else if (reaction == Reaction.LOW_MEDIUM) {
      if (security == Security.LOW || security == Security.LOW_MEDIUM) {
        result.setExecution(Execution.LOW);
      } else if (security == Security.MEDIUM_HIGH || security == Security.HIGH) {
        result.setExecution(Execution.LOW_MEDIUM);
      }
    } else if (reaction == Reaction.MEDIUM_HIGH) {
      if (security == Security.LOW || security == Security.LOW_MEDIUM) {
        result.setExecution(Execution.LOW_MEDIUM);
      } else if (security == Security.MEDIUM_HIGH || security == Security.HIGH) {
        result.setExecution(Execution.MEDIUM_HIGH);
      }
    } else if (reaction == Reaction.HIGH) {
      if (security == Security.LOW || security == Security.LOW_MEDIUM) {
        result.setExecution(Execution.LOW_MEDIUM);
      } else if (security == Security.MEDIUM_HIGH) {
        result.setExecution(Execution.MEDIUM_HIGH);
      } else if (security == Security.HIGH) {
        result.setExecution(Execution.HIGH);
      }
    }
  }

  public static void generateReaction(AvEvent event, AvResult result) {
    Lighting light = event.getEnvironmentalLighting();
    Integer tireness = event.getDriverTirenessLevel();

    if (light == Lighting.NA) {
      result.setReaction(Reaction.LOW);
    } else if (light == Lighting.BAD) {
      if (tireness < 50) {
        result.setReaction(Reaction.LOW_MEDIUM);
      } else if (tireness < 100) {
        result.setReaction(Reaction.LOW);
      } else {
        throw new MissingCaseException();
      }
    } else if (light == Lighting.SUFFICIENT) {
      if (tireness < 25) {
        result.setReaction(Reaction.MEDIUM_HIGH);
      } else if (tireness < 75) {
        result.setReaction(Reaction.LOW_MEDIUM);
      } else if (tireness < 100) {
        result.setReaction(Reaction.LOW);
      } else {
        throw new MissingCaseException();
      }
    } else if (light == Lighting.GOOD) {
      if (tireness < 50) {
        result.setReaction(Reaction.MEDIUM_HIGH);
      } else if (tireness < 75) {
        result.setReaction(Reaction.LOW_MEDIUM);
      } else if (tireness < 100) {
        result.setReaction(Reaction.LOW);
      } else {
        throw new MissingCaseException();
      }
    } else if (light == Lighting.VERY_GOOD) {
      if (tireness < 50) {
        result.setReaction(Reaction.MEDIUM_HIGH);
      } else if (tireness < 100) {
        result.setReaction(Reaction.LOW_MEDIUM);
      } else {
        throw new MissingCaseException();
      }
    } else if (light == Lighting.EXCELENT) {
      if (tireness < 25) {
        result.setReaction(Reaction.HIGH);
      } else if (tireness < 50) {
        result.setReaction(Reaction.MEDIUM_HIGH);
      } else if (tireness < 100) {
        result.setReaction(Reaction.LOW_MEDIUM);
      } else {
        throw new MissingCaseException();
      }
    }
  }

  public static void generateRiskAnalysis(AvEvent event, AvResult result) {
    Security security = result.getSecurity();
    Integer speed = event.getSpeed();
    Integer traction = event.getTractionWheelsLevel();

    if (security == Security.LOW) {
      if (traction < 25) {
        result.setRiskAnalysis(Risk.HIGH);
      } else if (traction < 100) {
        result.setRiskAnalysis(Risk.MEDIUM_HIGH);
      } else {
        throw new MissingCaseException();
      }
    } else if (security == Security.LOW_MEDIUM) {
      if (speed < 30) {
        result.setRiskAnalysis(Risk.MEDIUM_HIGH);
      } else if (speed < 120) {
        if (traction < 25) {
          result.setRiskAnalysis(Risk.HIGH);
        } else if (traction < 100) {
          result.setRiskAnalysis(Risk.MEDIUM_HIGH);
        } else {
          throw new MissingCaseException();
        }
      } else {
        throw new MissingCaseException();
      }
    } else if (security == Security.MEDIUM_HIGH) {
      if (speed < 30 || (speed >= 60 && speed < 120)) {
        if (traction < 25) {
          result.setRiskAnalysis(Risk.MEDIUM_HIGH);
        } else if (traction < 100) {
          result.setRiskAnalysis(Risk.LOW_MEDIUM);
        } else {
          throw new MissingCaseException();
        }
      } else if (speed < 60) {
        if (traction < 25) {
          result.setRiskAnalysis(Risk.MEDIUM_HIGH);
        } else if (traction < 75) {
          result.setRiskAnalysis(Risk.LOW_MEDIUM);
        } else if (traction < 100) {
          result.setRiskAnalysis(Risk.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else {
        throw new MissingCaseException();
      }
    } else if (security == Security.HIGH) {
      if (speed < 60) {
        if (traction < 50) {
          result.setRiskAnalysis(Risk.LOW_MEDIUM);
        } else if (traction < 100) {
          result.setRiskAnalysis(Risk.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (speed < 90) {
        result.setRiskAnalysis(Risk.LOW_MEDIUM);
      } else if (speed < 120) {
        if (traction < 25) {
          result.setRiskAnalysis(Risk.MEDIUM_HIGH);
        } else if (traction < 100) {
          result.setRiskAnalysis(Risk.LOW_MEDIUM);
        } else {
          throw new MissingCaseException();
        }
      } else {
        throw new MissingCaseException();
      }
    }
  }

  public static void generateIdentification(AvEvent event, AvResult result) {
    Rain rain = event.getRainConditions();
    Fog fog = event.getFogConditions();
    Integer tireness = event.getDriverTirenessLevel();

    if (rain == Rain.NA) {
      if (tireness < 50) {
        result.setIdentification(Identification.LOW_MEDIUM);
      } else if (tireness < 100) {
        result.setIdentification(Identification.LOW);
      } else {
        throw new MissingCaseException();
      }
    } else if (rain == Rain.LIGHT) {
      if (fog == Fog.NA) {
        if (tireness < 50) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.NONE) {
        if (tireness < 50) {
          result.setIdentification(Identification.HIGH);
        } else if (tireness < 75) {
          result.setIdentification(Identification.MEDIUM_HIGH);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.LIGHT) {
        if (tireness < 25) {
          result.setIdentification(Identification.HIGH);
        } else if (tireness < 50) {
          result.setIdentification(Identification.MEDIUM_HIGH);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.MEDIUM) {
        if (tireness < 50) {
          result.setIdentification(Identification.MEDIUM_HIGH);
        } else if (tireness < 75) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.DENSE) {
        if (tireness < 50) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      }
    } else if (rain == Rain.MEDIUM) {
      if (fog == Fog.NA || fog == Fog.MEDIUM) {
        if (tireness < 50) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.NONE || fog == Fog.LIGHT) {
        if (tireness < 75) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.DENSE) {
        result.setIdentification(Identification.LOW);
      }
    } else if (rain == Rain.HEAVY) {
      if (fog == Fog.NA) {
        if (tireness < 50) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.NONE || fog == Fog.LIGHT) {
        if (tireness < 25) {
          result.setIdentification(Identification.LOW_MEDIUM);
        } else if (tireness < 100) {
          result.setIdentification(Identification.LOW);
        } else {
          throw new MissingCaseException();
        }
      } else if (fog == Fog.MEDIUM || fog == Fog.DENSE) {
        result.setIdentification(Identification.LOW);
      }
    }
  }

  public static void generateDetection(AvEvent event, AvResult result) {
    Integer speed = event.getSpeed();
    Integer tireness = event.getDriverTirenessLevel();

    if (speed < 30) {
      if (tireness < 25) {
        result.setDetection(Detection.HIGH);
      } else if (tireness < 75) {
        result.setDetection(Detection.MEDIUM_HIGH);
      } else if (tireness < 100) {
        result.setDetection(Detection.LOW_MEDIUM);
      } else {
        throw new MissingCaseException();
      }
    } else if (speed < 60) {
      if (tireness < 50) {
        result.setDetection(Detection.MEDIUM_HIGH);
      } else if (tireness < 100) {
        result.setDetection(Detection.LOW_MEDIUM);
      } else {
        throw new MissingCaseException();
      }
    } else if (speed < 90) {
      if (tireness < 25) {
        result.setDetection(Detection.MEDIUM_HIGH);
      } else if (tireness < 75) {
        result.setDetection(Detection.LOW_MEDIUM);
      } else if (tireness < 100) {
        result.setDetection(Detection.LOW);
      } else {
        throw new MissingCaseException();
      }
    } else if (speed < 120) {
      if (tireness < 50) {
        result.setDetection(Detection.LOW_MEDIUM);
      } else if (tireness < 100) {
        result.setDetection(Detection.LOW);
      } else {
        throw new MissingCaseException();
      }
    } else {
      throw new MissingCaseException();
    }
  }

  public static void generateSocialResponsibility(AvResult result) {
    Utilitarianism util = result.getUtilitarianism();
    Legitimacy legit = result.getLegitimacy();

    if (legit == Legitimacy.NOT_FOLLOWING) {
      result.setSocialResponsibility(Responsibility.BAD);
    } else if (util == Utilitarianism.LOW) {
      result.setSocialResponsibility(Responsibility.MEDIUM);
    } else if (util == Utilitarianism.LOW_MEDIUM || util == Utilitarianism.MEDIUM_HIGH) {
      result.setSocialResponsibility(Responsibility.MEDIUM);
    } else if (util == Utilitarianism.HIGH) {
      result.setSocialResponsibility(Responsibility.GOOD);
    } else {
      throw new MissingCaseException();
    }
  }

  public static void generateUtilitarianism(AvEvent event, AvResult result) {
    Integer tireness = event.getDriverTirenessLevel();

    if (tireness < 25) {
      result.setUtilitarianism(Utilitarianism.LOW);
    } else if (tireness < 50) {
      result.setUtilitarianism(Utilitarianism.LOW_MEDIUM);
    } else if (tireness < 75) {
      result.setUtilitarianism(Utilitarianism.MEDIUM_HIGH);
    } else if (tireness < 100) {
      result.setUtilitarianism(Utilitarianism.HIGH);
    } else {
      throw new MissingCaseException();
    }
  }

  public static void generateSecurity(AvResult result) {
    Infrastructure inf = result.getICTInfrastructure();
    Road road = result.getRoadConditions();

    if (inf == Infrastructure.BAD && (road == Road.BAD || road == Road.MEDIUM)) {
      result.setSecurity(Security.LOW);
    } else if ((inf == Infrastructure.BAD && road == Road.GOOD)
        || (inf == Infrastructure.MEDIUM && (road == Road.BAD || road == Road.MEDIUM))
        || (inf == Infrastructure.GOOD && road == Road.BAD)) {
      result.setSecurity(Security.LOW_MEDIUM);
    } else if ((inf == Infrastructure.MEDIUM && road == Road.GOOD)
        || (inf == Infrastructure.GOOD && road == Road.MEDIUM)) {
      result.setSecurity(Security.MEDIUM_HIGH);
    } else if (inf == Infrastructure.GOOD && road == Road.GOOD) {
      result.setSecurity(Security.HIGH);
    } else {
      throw new MissingCaseException();
    }
  }

  // Generate random enum values
  public static <T extends Enum<?>> T randomEnum(Class<T> cls) {
    int x = RANDOM.nextInt(cls.getEnumConstants().length);
    return cls.getEnumConstants()[x];
  }
}
