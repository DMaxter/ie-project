package pt.tecnico.avaas.pilot;

import java.util.Date;

public class AvResult {
  private Long id;
  private Identification identification;
  private Date timestamp;
  private Infrastructure ICTInfrastructure;
  private Road roadConditions;
  private Security security;
  private Risk riskAnalysis;
  private Reaction reaction;
  private Execution execution;
  private Detection detection;
  private Utilitarianism utilitarianism;
  private Legitimacy legitimacy;
  private Responsibility socialResponsibility;
  private Integer carId;
  private Integer apilotId;
  private Integer userId;

  public AvResult() {
  }

  public Identification getIdentification() {
    return identification;
  }

  public void setIdentification(Identification identification) {
    this.identification = identification;
  }

  public Risk getRiskAnalysis() {
    return riskAnalysis;
  }

  public void setRiskAnalysis(Risk riskAnalysis) {
    this.riskAnalysis = riskAnalysis;
  }

  public Reaction getReaction() {
    return reaction;
  }

  public void setReaction(Reaction reaction) {
    this.reaction = reaction;
  }

  public Execution getExecution() {
    return execution;
  }

  public void setExecution(Execution execution) {
    this.execution = execution;
  }

  public Detection getDetection() {
    return detection;
  }

  public void setDetection(Detection detection) {
    this.detection = detection;
  }

  public Utilitarianism getUtilitarianism() {
    return utilitarianism;
  }

  public void setUtilitarianism(Utilitarianism utilitarianism) {
    this.utilitarianism = utilitarianism;
  }

  public Legitimacy getLegitimacy() {
    return legitimacy;
  }

  public void setLegitimacy(Legitimacy legitimacy) {
    this.legitimacy = legitimacy;
  }

  public Responsibility getSocialResponsibility() {
    return socialResponsibility;
  }

  public void setSocialResponsibility(Responsibility socialResponsibility) {
    this.socialResponsibility = socialResponsibility;
  }

  public Infrastructure getICTInfrastructure() {
    return ICTInfrastructure;
  }

  public void setICTInfrastructure(Infrastructure ICTInfrastructure) {
    this.ICTInfrastructure = ICTInfrastructure;
  }

  public Road getRoadConditions() {
    return roadConditions;
  }

  public void setRoadConditions(Road road_Conditions) {
    this.roadConditions = road_Conditions;
  }

  public Security getSecurity() {
    return security;
  }

  public void setSecurity(Security security) {
    this.security = security;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getCarId() {
    return carId;
  }

  public void setCarId(Integer carId) {
    this.carId = carId;
  }

  public Integer getApilotId() {
    return apilotId;
  }

  public void setApilotId(Integer apilotId) {
    this.apilotId = apilotId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public enum Road {
    BAD("Bad Road"),
    MEDIUM("Medium Conditions Road"),
    GOOD("Good Road");

    private final String text;

    Road(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Security {
    LOW("Low Safety"),
    LOW_MEDIUM("Low-Medium Safety"),
    MEDIUM_HIGH("Medium-High Safety"),
    HIGH("High Safety");

    private final String text;

    Security(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }

  }

  public enum Risk {
    LOW("Low Risk"),
    LOW_MEDIUM("Low-Medium Risk"),
    MEDIUM_HIGH("Medium-High Risk"),
    HIGH("High Risk");

    private final String text;

    Risk(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Infrastructure {
    BAD("Bad"),
    MEDIUM("Medium"),
    GOOD("Good");

    private final String text;

    Infrastructure(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Utilitarianism {
    LOW("Low Happiness"),
    LOW_MEDIUM("Low-Medium Happiness"),
    MEDIUM_HIGH("Medium-High Happiness"),
    HIGH("High Happiness");

    private final String text;

    Utilitarianism(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Legitimacy {
    FOLLOWING("Following Rules"),
    NOT_FOLLOWING("Not Following Rules");

    private final String text;

    Legitimacy(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Responsibility {
    BAD("Not Responsible"),
    MEDIUM("Medium Responsible"),
    GOOD("Responsible");

    private final String text;

    Responsibility(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Detection {
    LOW("Low Detection"),
    LOW_MEDIUM("Low-Medium Detection"),
    MEDIUM_HIGH("Medium-High Detection"),
    HIGH("High Detection");

    private final String text;

    Detection(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Identification {
    LOW("Low Identification"),
    LOW_MEDIUM("Low-Medium Identification"),
    MEDIUM_HIGH("Medium-High Identification"),
    HIGH("High Identification");

    private final String text;

    Identification(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Reaction {
    LOW("Low Reaction"),
    LOW_MEDIUM("Low-Medium Reaction"),
    MEDIUM_HIGH("Medium-High Reaction"),
    HIGH("High Reaction");

    private final String text;

    Reaction(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Execution {
    LOW("Low Execution"),
    LOW_MEDIUM("Low-Medium Execution"),
    MEDIUM_HIGH("Medium-High Execution"),
    HIGH("High Execution");

    private final String text;

    Execution(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }
}