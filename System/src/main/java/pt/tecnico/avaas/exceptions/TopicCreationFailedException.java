package pt.tecnico.avaas.exceptions;

public class TopicCreationFailedException extends Exception {
  private String topic;

  public TopicCreationFailedException(String topic) {
    this.topic = topic;
  }

  public String getTopic() {
    return topic;
  }
}
