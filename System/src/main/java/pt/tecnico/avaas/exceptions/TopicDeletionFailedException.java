package pt.tecnico.avaas.exceptions;

public class TopicDeletionFailedException extends Exception {
  private String topic;

  public TopicDeletionFailedException(String topic) {
    this.topic = topic;
  }

  public String getTopic() {
    return topic;
  }
}
