package pt.tecnico.avaas.dto;

import java.util.HashMap;
import java.util.Map;

public class CamundaDto {
  public static abstract class TypeVariable {}

  public static abstract class Type<T> extends TypeVariable {
    private String type;
    private T value;

    Type(String type, T value) {
      this.type = type;
      this.value = value;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public T getValue() {
      return value;
    }

    public void setValue(T value) {
      this.value = value;
    }
  }

  public class IntegerT extends Type<Integer> {
    public static final String type = "Integer";

    IntegerT() {
      super(type, 0);
    }

    IntegerT(int i) {
      super(type, i);
    }
  }

  public class StringT extends Type<String> {
    public static final String type = "String";

    StringT() {
      super(type, "");
    }

    StringT(String s) {
      super(type, s);
    }
  }

  public class BooleanT extends Type<Boolean> {
    public static final String type = "Boolean";

    BooleanT() {
      super(type, false);
    }

    BooleanT(boolean b) {
      super(type, b);
    }
  }

  public final String messageName = "AVResult";

  private Map<String, TypeVariable> processVariables = new HashMap<>();

  public CamundaDto() {
  }

  public String getMessageName() {
    return messageName;
  }

  public Map<String, TypeVariable> getProcessVariables() {
    return processVariables;
  }

  public void setProcessVariables(AvResultDto result) {
    this.processVariables.put("identification", new StringT(result.getIdentification().name()));
    this.processVariables.put("timestamp", new StringT(result.getTimestamp().toString()));
    this.processVariables.put("ictinfrastructure", new StringT(result.getICTInfrastructure().name()));
    this.processVariables.put("roadConditions", new StringT(result.getRoadConditions().name()));
    this.processVariables.put("security", new StringT(result.getSecurity().name()));
    this.processVariables.put("riskAnalysis", new StringT(result.getRiskAnalysis().name()));
    this.processVariables.put("reaction", new StringT(result.getReaction().name()));
    this.processVariables.put("execution", new StringT(result.getExecution().name()));
    this.processVariables.put("detection", new StringT(result.getDetection().name()));
    this.processVariables.put("utilitarianism", new StringT(result.getUtilitarianism().name()));
    this.processVariables.put("legitimacy", new StringT(result.getLegitimacy().name()));
    this.processVariables.put("socialResponsibility", new StringT(result.getSocialResponsibility().name()));
    this.processVariables.put("carId", new IntegerT(result.getCarId()));
    this.processVariables.put("apilotId", new IntegerT(result.getApilotId()));
    this.processVariables.put("userId", new IntegerT(result.getUserId()));

    this.processVariables.put("aqsubscribed",
        new BooleanT(result.isAqsubscribed()));
    this.processVariables.put("iqsubscribed",
        new BooleanT(result.isIqsubscribed()));
    this.processVariables.put("eqsubscribed",
        new BooleanT(result.isEqsubscribed()));
  }



}
