package pt.tecnico.avaas.dto;

import pt.tecnico.avaas.model.User;

public class UserDto {
  private Integer id;
  private CarDto car;
  private ApilotDto apilot;
  private String name;
  private double balance;
  private boolean aq;
  private boolean eq;
  private boolean iq;

  public UserDto() {}

  public UserDto(User user) {
    this.id = user.getId();
    this.name = user.getName();
    this.balance = user.getBalance();
    this.aq = user.getAq();
    this.eq = user.getEq();
    this.iq = user.getIq();

    if (user.getCar() != null) {
      this.car = new CarDto(user.getCar());
    }

    if (user.getApilot() != null) {
      this.apilot = new ApilotDto(user.getApilot());
    }
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public CarDto getCar() {
    return car;
  }

  public void setCar(CarDto car) {
    this.car = car;
  }

  public ApilotDto getApilot() {
    return apilot;
  }

  public void setApilot(ApilotDto apilot) {
    this.apilot = apilot;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  public boolean getAq() {
    return aq;
  }

  public void setAq(boolean aq) {
    this.aq = aq;
  }

  public boolean getEq() {
    return eq;
  }

  public void setEq(boolean eq) {
    this.eq = eq;
  }

  public boolean getIq() {
    return iq;
  }

  public void setIq(boolean iq) {
    this.iq = iq;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((apilot == null) ? 0 : apilot.hashCode());
    result = prime * result + (aq ? 1231 : 1237);
    long temp;
    temp = Double.doubleToLongBits(balance);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((car == null) ? 0 : car.hashCode());
    result = prime * result + (eq ? 1231 : 1237);
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + (iq ? 1231 : 1237);
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    UserDto other = (UserDto) obj;
    if (apilot == null) {
      if (other.apilot != null)
        return false;
    } else if (!apilot.equals(other.apilot))
      return false;
    if (aq != other.aq)
      return false;
    if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
      return false;
    if (car == null) {
      if (other.car != null)
        return false;
    } else if (!car.equals(other.car))
      return false;
    if (eq != other.eq)
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (iq != other.iq)
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }
}
