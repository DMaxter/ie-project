package pt.tecnico.avaas.dto;

import java.util.Date;

import pt.tecnico.avaas.model.AvResult.*;

public class AvResultDto {
  private Long id;
  private Identification identification;
  private Date timestamp;
  private Infrastructure ICTInfrastructure;
  private Road roadConditions;
  private Security security;
  private Risk riskAnalysis;
  private Reaction reaction;
  private Execution execution;
  private Detection detection;
  private Utilitarianism utilitarianism;
  private Legitimacy legitimacy;
  private Responsibility socialResponsibility;
  private Integer carId;
  private Integer apilotId;
  private Integer userId;
  private boolean aqsubscribed;
  private boolean eqsubscribed;
  private boolean iqsubscribed;

  public AvResultDto() {}

  public Identification getIdentification() {
    return identification;
  }
  public void setIdentification(Identification identification) {
    this.identification = identification;
  }
  public Risk getRiskAnalysis() {
    return riskAnalysis;
  }
  public void setRiskAnalysis(Risk riskAnalysis) {
    this.riskAnalysis = riskAnalysis;
  }
  public Reaction getReaction() {
    return reaction;
  }
  public void setReaction(Reaction reaction) {
    this.reaction = reaction;
  }
  public Execution getExecution() {
    return execution;
  }
  public void setExecution(Execution execution) {
    this.execution = execution;
  }
  public Detection getDetection() {
    return detection;
  }
  public void setDetection(Detection detection) {
    this.detection = detection;
  }
  public Utilitarianism getUtilitarianism() {
    return utilitarianism;
  }
  public void setUtilitarianism(Utilitarianism utilitarianism) {
    this.utilitarianism = utilitarianism;
  }
  public Legitimacy getLegitimacy() {
    return legitimacy;
  }
  public void setLegitimacy(Legitimacy legitimacy) {
    this.legitimacy = legitimacy;
  }
  public Responsibility getSocialResponsibility() {
    return socialResponsibility;
  }
  public void setSocialResponsibility(Responsibility socialResponsibility) {
    this.socialResponsibility = socialResponsibility;
  }
  public Infrastructure getICTInfrastructure() {
    return ICTInfrastructure;
  }
  public void setICTInfrastructure(Infrastructure ICTInfrastructure) {
    this.ICTInfrastructure = ICTInfrastructure;
  }
  public Road getRoadConditions() {
    return roadConditions;
  }
  public void setRoadConditions(Road road_Conditions) {
    this.roadConditions = road_Conditions;
  }
  public Security getSecurity() {
    return security;
  }
  public void setSecurity(Security security) {
    this.security = security;
  }
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getCarId() {
    return carId;
  }

  public void setCarId(Integer carId) {
    this.carId = carId;
  }

  public Integer getApilotId() {
    return apilotId;
  }

  public void setApilotId(Integer apilotId) {
    this.apilotId = apilotId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public boolean isAqsubscribed() {
    return aqsubscribed;
  }

  public void setAqsubscribed(boolean aqsubscribed) {
    this.aqsubscribed = aqsubscribed;
  }

  public boolean isEqsubscribed() {
    return eqsubscribed;
  }

  public void setEqsubscribed(boolean eqsubscribed) {
    this.eqsubscribed = eqsubscribed;
  }

  public boolean isIqsubscribed() {
    return iqsubscribed;
  }

  public void setIqsubscribed(boolean iqsubscribed) {
    this.iqsubscribed = iqsubscribed;
  }
}
