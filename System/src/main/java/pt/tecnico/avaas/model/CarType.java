package pt.tecnico.avaas.model;

public enum CarType {
    SEDAN,
    SUV,
    TRUCK,
    BUS,
    VAN,
}