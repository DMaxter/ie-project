package pt.tecnico.avaas.model;

import javax.persistence.*;

import pt.tecnico.avaas.dto.UserDto;

@Entity
@Table(name = "users")
@NamedQuery(name = "Users.findAll", query = "FROM User")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(nullable = false)
  private String name;

  private boolean aq;
  private boolean iq;
  private boolean eq;

  private double balance;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(referencedColumnName = "id", name = "car_id")
  private Car car;


  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(referencedColumnName = "id", name = "apilot_id")
  private Apilot apilot;

  public User() {}

  public User(UserDto user) {
    this.id = user.getId();
    this.name = user.getName();
    this.balance = user.getBalance();
    this.aq = user.getAq();
    this.eq = user.getEq();
    this.iq = user.getIq();


    if (user.getCar() != null) {
      this.car = new Car(user.getCar());
    }

    if (user.getApilot() != null) {
      this.apilot = new Apilot(user.getApilot());
    }
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean getEq() {
    return eq;
  }

  public void setEq(boolean eq) {
    this.eq = eq;
  }

  public boolean getAq() {
    return aq;
  }

  public void setAq(boolean aq) {
    this.aq = aq;
  }

  public boolean getIq() {
    return iq;
  }

  public void setIq(boolean iq) {
    this.iq = iq;
  }

  public Car getCar() {
    return car;
  }

  public void setCar(Car car) {
    this.car = car;
  }

  public Apilot getApilot() {
    return apilot;
  }

  public void setApilot(Apilot apilot) {
    this.apilot = apilot;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((apilot == null) ? 0 : apilot.hashCode());
    result = prime * result + (aq ? 1231 : 1237);
    long temp;
    temp = Double.doubleToLongBits(balance);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((car == null) ? 0 : car.hashCode());
    result = prime * result + (eq ? 1231 : 1237);
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + (iq ? 1231 : 1237);
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    User other = (User) obj;
    if (apilot == null) {
      if (other.apilot != null)
        return false;
    } else if (!apilot.equals(other.apilot))
      return false;
    if (aq != other.aq)
      return false;
    if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
      return false;
    if (car == null) {
      if (other.car != null)
        return false;
    } else if (!car.equals(other.car))
      return false;
    if (eq != other.eq)
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (iq != other.iq)
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }
}

