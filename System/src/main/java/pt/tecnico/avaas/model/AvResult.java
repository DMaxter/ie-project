package pt.tecnico.avaas.model;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import pt.tecnico.avaas.dto.AvResultDto;

@Entity
@Table(name = "avresults")
public class AvResult {
  @Id
  @GeneratedValue
  private Long id;

  private Date timestamp;

  @Embedded
  private Intelligence intelligence;

  @Embedded
  private Ethical ethical;

  @Embedded
  private Adversity adversity;

  @ManyToOne
  private User user;

  @ManyToOne
  private Car car;

  @ManyToOne
  private Apilot apilot;

  public AvResult() {
  }

  public AvResult(AvResultDto result, User user, Car car, Apilot apilot) {
    this.id = result.getId();

    this.user = user;
    this.car = car;
    this.apilot = apilot;
    this.timestamp = result.getTimestamp();

    this.intelligence = new Intelligence();
    this.intelligence.setDetection(result.getDetection());
    this.intelligence.setExecution(result.getExecution());
    this.intelligence.setIdentification(result.getIdentification());
    this.intelligence.setReaction(result.getReaction());
    this.intelligence.setRiskAnalysis(result.getRiskAnalysis());

    this.ethical = new Ethical();
    this.ethical.setLegitimacy(result.getLegitimacy());
    this.ethical.setSocialResponsibility(result.getSocialResponsibility());
    this.ethical.setUtilitarianism(result.getUtilitarianism());

    this.adversity = new Adversity();
    this.adversity.setICTInfrastructure(result.getICTInfrastructure());
    this.adversity.setRoadConditions(result.getRoadConditions());
    this.adversity.setSecurity(result.getSecurity());
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Intelligence getIntelligence() {
    return intelligence;
  }

  public void setIntelligence(Intelligence intelligence) {
    this.intelligence = intelligence;
  }

  public Ethical getEthical() {
    return ethical;
  }

  public void setEthical(Ethical ethical) {
    this.ethical = ethical;
  }

  public Adversity getAdversity() {
    return adversity;
  }

  public void setAdversity(Adversity adversity) {
    this.adversity = adversity;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public Car getCar() {
    return car;
  }

  public void setCar(Car car) {
    this.car = car;
  }

  public Apilot getApilot() {
    return apilot;
  }

  public void setApilot(Apilot apilot) {
    this.apilot = apilot;
  }

  @Embeddable
  public static class Intelligence {
    @Enumerated(EnumType.STRING)
    private Identification identification;
    @Enumerated(EnumType.STRING)
    private Risk riskAnalysis;
    @Enumerated(EnumType.STRING)
    private Reaction reaction;
    @Enumerated(EnumType.STRING)
    private Execution execution;
    @Enumerated(EnumType.STRING)
    private Detection detection;

    public Intelligence() {
    }

    public Identification getIdentification() {
      return identification;
    }

    public void setIdentification(Identification identification) {
      this.identification = identification;
    }

    public Risk getRiskAnalysis() {
      return riskAnalysis;
    }

    public void setRiskAnalysis(Risk riskAnalysis) {
      this.riskAnalysis = riskAnalysis;
    }

    public Reaction getReaction() {
      return reaction;
    }

    public void setReaction(Reaction reaction) {
      this.reaction = reaction;
    }

    public Execution getExecution() {
      return execution;
    }

    public void setExecution(Execution execution) {
      this.execution = execution;
    }

    public Detection getDetection() {
      return detection;
    }

    public void setDetection(Detection detection) {
      this.detection = detection;
    }
  }

  @Embeddable
  public static class Ethical {
    @Enumerated(EnumType.STRING)
    private Utilitarianism utilitarianism;
    @Enumerated(EnumType.STRING)
    private Legitimacy legitimacy;
    @Enumerated(EnumType.STRING)
    private Responsibility socialResponsibility;

    public Ethical() {
    }

    public Utilitarianism getUtilitarianism() {
      return utilitarianism;
    }

    public void setUtilitarianism(Utilitarianism utilitarianism) {
      this.utilitarianism = utilitarianism;
    }

    public Legitimacy getLegitimacy() {
      return legitimacy;
    }

    public void setLegitimacy(Legitimacy legitimacy) {
      this.legitimacy = legitimacy;
    }

    public Responsibility getSocialResponsibility() {
      return socialResponsibility;
    }

    public void setSocialResponsibility(Responsibility socialResponsibility) {
      this.socialResponsibility = socialResponsibility;
    }
  }

  @Embeddable
  public static class Adversity {
    @Enumerated(EnumType.STRING)
    private Infrastructure ICTInfrastructure;
    @Enumerated(EnumType.STRING)
    private Road roadConditions;
    @Enumerated(EnumType.STRING)
    private Security security;

    public Adversity() {
    }

    public Infrastructure getICTInfrastructure() {
      return ICTInfrastructure;
    }

    public void setICTInfrastructure(Infrastructure ICTInfrastructure) {
      this.ICTInfrastructure = ICTInfrastructure;
    }

    public Road getRoadConditions() {
      return roadConditions;
    }

    public void setRoadConditions(Road roadConditions) {
      this.roadConditions = roadConditions;
    }

    public Security getSecurity() {
      return security;
    }

    public void setSecurity(Security security) {
      this.security = security;
    }
  }

  public enum Road {
    BAD("Bad Road"),
    MEDIUM("Medium Conditions Road"),
    GOOD("Good Road");

    private final String text;

    Road(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Security {
    LOW("Low Safety"),
    LOW_MEDIUM("Low-Medium Safety"),
    MEDIUM_HIGH("Medium-High Safety"),
    HIGH("High Safety");

    private final String text;

    Security(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }

  }

  public enum Risk {
    LOW("Low Risk"),
    LOW_MEDIUM("Low-Medium Risk"),
    MEDIUM_HIGH("Medium-High Risk"),
    HIGH("High Risk");

    private final String text;

    Risk(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Infrastructure {
    BAD("Bad"),
    MEDIUM("Medium"),
    GOOD("Good");

    private final String text;

    Infrastructure(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Utilitarianism {
    LOW("Low Happiness"),
    LOW_MEDIUM("Low-Medium Happiness"),
    MEDIUM_HIGH("Medium-High Happiness"),
    HIGH("High Happiness");

    private final String text;

    Utilitarianism(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Legitimacy {
    FOLLOWING("Following Rules"),
    NOT_FOLLOWING("Not Following Rules");

    private final String text;

    Legitimacy(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Responsibility {
    BAD("Not Responsible"),
    MEDIUM("Medium Responsible"),
    GOOD("Responsible");

    private final String text;

    Responsibility(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Detection {
    LOW("Low Detection"),
    LOW_MEDIUM("Low-Medium Detection"),
    MEDIUM_HIGH("Medium-High Detection"),
    HIGH("High Detection");

    private final String text;

    Detection(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Identification {
    LOW("Low Identification"),
    LOW_MEDIUM("Low-Medium Identification"),
    MEDIUM_HIGH("Medium-High Identification"),
    HIGH("High Identification");

    private final String text;

    Identification(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Reaction {
    LOW("Low Reaction"),
    LOW_MEDIUM("Low-Medium Reaction"),
    MEDIUM_HIGH("Medium-High Reaction"),
    HIGH("High Reaction");

    private final String text;

    Reaction(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }

  public enum Execution {
    LOW("Low Execution"),
    LOW_MEDIUM("Low-Medium Execution"),
    MEDIUM_HIGH("Medium-High Execution"),
    HIGH("High Execution");

    private final String text;

    Execution(String text) {
      this.text = text;
    }

    @Override
    public String toString() {
      return text;
    }
  }
}
