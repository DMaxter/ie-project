package pt.tecnico.avaas.serialization;

import java.io.IOException;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import pt.tecnico.avaas.dto.AvEvent;
import pt.tecnico.avaas.dto.AvEvent.Fog;
import pt.tecnico.avaas.dto.AvEvent.Lighting;
import pt.tecnico.avaas.dto.AvEvent.Rain;

public class AvEventDeserializer extends StdDeserializer<AvEvent> {
  public AvEventDeserializer() {
    this(null);
  }

  public AvEventDeserializer(Class<?> c) {
    super(c);
  }

  @Override
  public AvEvent deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException, IllegalArgumentException {
    JsonNode event = p.getCodec().readTree(p);
    JsonNode node = event.get("AV_Event");

    String id = node.get("AV_ID").asText();
    String timestamp = node.get("TimeStamp").asText();
    Integer speed = node.get("Speed").intValue();
    Integer battery = node.get("BatteryLevel").intValue();
    Integer driver = node.get("DriverTirenessLevel").intValue();
    String location = node.get("Location").asText();
    Lighting light = Lighting.getEnum(node.get("EnvironmentalLightning").asText());
    Rain rain = Rain.getEnum(node.get("RainConditions").asText());
    Fog fog = Fog.getEnum(node.get("FogConditions").asText());
    Integer traction = node.get("TractionWheelsLevel").intValue();

    return new AvEvent(timestamp, id, speed, battery, driver, location, light, rain, fog, traction);
  }
}
