package pt.tecnico.avaas.services;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import io.smallrye.mutiny.Uni;
import pt.tecnico.avaas.dto.CamundaDto;

@RegisterRestClient
@Path("/engine-rest")
public interface CamundaService {
  @POST
  @Path("/message")
  Uni<Void> sendMessage(CamundaDto dto);
}
