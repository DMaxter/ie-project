package pt.tecnico.avaas.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import io.smallrye.common.annotation.Identifier;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.kafka.KafkaClientService;
import io.smallrye.reactive.messaging.kafka.KafkaProducer;
import pt.tecnico.avaas.dto.AvEvent;
import pt.tecnico.avaas.dto.AvResultDto;
import pt.tecnico.avaas.dto.CamundaDto;
import pt.tecnico.avaas.exceptions.TopicCreationFailedException;
import pt.tecnico.avaas.exceptions.TopicDeletionFailedException;
import pt.tecnico.avaas.model.Apilot;
import pt.tecnico.avaas.model.AvResult;
import pt.tecnico.avaas.model.Car;
import pt.tecnico.avaas.model.User;
import pt.tecnico.avaas.serialization.AvEventDeserializer;

@ApplicationScoped
public class KafkaService {
  private static final Logger LOGGER = Logger.getLogger(KafkaService.class);
  private static final String CAR_PATTERN = "car(\\d+)";
  private static final String PILOT_CHANNEL = "pilots";
  private static final String USER_CHANNEL = "users";

  @Inject
  @Identifier("default-kafka-broker")
  Map<String, Object> config;

  @Inject
  @ConfigProperty(name = "kafka.bootstrap.servers")
  List<String> servers;

  @Inject
  KafkaClientService clientService;

  @Inject
  Mutiny.SessionFactory client;

  @Inject
  @RestClient
  CamundaService restClient;

  private AdminClient getAdminClient() {
    Map<String, Object> copy = new HashMap<>();
    for (Map.Entry<String, Object> entry : config.entrySet()) {
      if (AdminClientConfig.configNames().contains(entry.getKey())) {
        copy.put(entry.getKey(), entry.getValue());
      }
    }

    return KafkaAdminClient.create(copy);
  }

  public void createTopic(String name) throws TopicCreationFailedException {
    NewTopic topic = new NewTopic(name, servers.size() * 2, (short) servers.size());

    CreateTopicsResult result = getAdminClient().createTopics(Arrays.asList(topic));

    try {
      result.all().get();
    } catch (ExecutionException | InterruptedException e) {
      throw new TopicCreationFailedException(name);
    }
  }

  public void deleteTopic(String name) throws TopicDeletionFailedException {
    DeleteTopicsResult result = getAdminClient().deleteTopics(Arrays.asList(name));

    try {
      result.all().get();
    } catch (ExecutionException | InterruptedException e) {
      throw new TopicDeletionFailedException(name);
    }
  }

  public Uni<Object> sendToUserTopic(String topic, String actions) {
      KafkaProducer<String, String> producer = clientService.getProducer(USER_CHANNEL);
      return producer.runOnSendingThread(client -> {
        return client.send(new ProducerRecord<>(topic, actions));
      });
  }

  @Incoming("cars")
  public void redirectToApilot(String json) {
    ObjectMapper om = new ObjectMapper();
    SimpleModule module = new SimpleModule("AvEventDeserializer", new Version(1, 0, 0, null, null, null));
    module.addDeserializer(AvEvent.class, new AvEventDeserializer());
    om.registerModule(module);

    try {
      AvEvent event = om.readValue(json, AvEvent.class);

      // Get car id
      Pattern pattern = Pattern.compile(CAR_PATTERN);
      Matcher matcher = pattern.matcher(event.getAVID());

      if (matcher.find()) {
        Integer carId;

        try {
          carId = Integer.parseInt(matcher.group(1));
        } catch (NumberFormatException e) {
          LOGGER.error("Invalid AV_ID " + event.getAVID());
          return;
        }

        this.client.withTransaction((s, t) -> s.find(Car.class, carId)).subscribe().with(
            car -> {
              // Check if car exists
              if (car == null) {
                LOGGER.error("Car " + carId + " not found");
                return;
              }

              // Check if car is owned
              User user = car.getUser();
              if (user == null) {
                LOGGER.error("Car " + carId + " is not owned by any user");
                return;
              }

              // Check if user has a pilot
              Apilot pilot = user.getApilot();
              if (pilot == null) {
                LOGGER.error("User " + user.getId() + " with car " + carId + " doesn't have an apilot");
                return;
              }

              event.setApilotId(pilot.getId());
              event.setUserId(user.getId());
              event.setCarId(car.getId());

              // Send to pilot
              KafkaProducer<String, AvEvent> producer = clientService.getProducer(PILOT_CHANNEL);
              producer.runOnSendingThread(client -> {
                return client.send(new ProducerRecord<>(ApilotService.TOPIC_PREFIX + pilot.getId(), event));
              }).subscribe().with(item -> LOGGER.info("Sent event from car " + carId + " to pilot " + pilot.getId()),
                  failure -> LOGGER.error("Error sending event to pilot " + failure.getMessage()));
            }, e -> {
              LOGGER.error("Error on transaction: " + e.getMessage());
            });
      } else {
        LOGGER.error("Invalid AV_ID " + event.getAVID());
      }

    } catch (JsonProcessingException | IllegalArgumentException e) {
      LOGGER.error("Could not process JSON: " + e.getMessage());
    }
  }

  @Incoming("avresults")
  public void saveAvResult(AvResultDto result) {
    LOGGER.info("Received result from pilot " + result.getApilotId() + " and car " + result.getCarId());

    this.client.withTransaction((s, t) -> {
      Uni<Apilot> apilotUni = s.find(Apilot.class, result.getApilotId());
      Uni<Car> carUni = s.find(Car.class, result.getCarId());
      Uni<User> userUni = s.find(User.class, result.getUserId());

      return Uni.combine().all().unis(apilotUni, carUni, userUni).asTuple().onItem().transformToUni(tuple -> {
        Apilot apilot = tuple.getItem1();
        Car car = tuple.getItem2();
        User user = tuple.getItem3();

        if (user == null) {
          LOGGER.error("User " + result.getUserId() + " doesn't exist");
          return Uni.createFrom().nothing();
        } else if (car == null) {
          LOGGER.error("Car " + result.getCarId() + " doesn't exist");
          return Uni.createFrom().nothing();
        } else if (apilot == null) {
          LOGGER.error("Apilot " + result.getApilotId() + " doesn't exist");
          return Uni.createFrom().nothing();
        }

        result.setId(null);
        result.setAqsubscribed(user.getAq());
        result.setEqsubscribed(user.getEq());
        result.setIqsubscribed(user.getIq());

        return s.persist(new AvResult(result, user, car, apilot));
      });
    }).onItem().transformToUni(item -> {
      LOGGER.info("Saved AVResult from car " + result.getCarId() + " and apilot " + result.getApilotId());

      CamundaDto payload = new CamundaDto();
      payload.setProcessVariables(result);

      return restClient.sendMessage(payload);
    }).subscribe().with(item -> LOGGER.info("Sent to camunda"), failure -> LOGGER.error("Error submitting to camunda:" + failure.getMessage()));
  }
}
