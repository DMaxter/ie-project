package pt.tecnico.avaas.services;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.reactive.mutiny.Mutiny;
import org.hibernate.reactive.mutiny.Mutiny.Query;
import org.jboss.logging.Logger;

import io.smallrye.common.constraint.NotNull;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import pt.tecnico.avaas.dto.CarDto;
import pt.tecnico.avaas.exceptions.TopicCreationFailedException;
import pt.tecnico.avaas.exceptions.TopicDeletionFailedException;
import pt.tecnico.avaas.model.Car;
import pt.tecnico.avaas.model.CarType;


@Path("car")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class ManufacturerService {
  private static final Logger LOGGER = Logger.getLogger(ManufacturerService.class);
  public static final String TOPIC_PREFIX = "car";

    @Inject
    Mutiny.SessionFactory client;

    @Inject
    KafkaService kafkaService;

    @GET
    public Multi<CarDto> getAll() {
      return this.client.withTransaction((s,t) -> {
              Query<Car> query = s.createNamedQuery("Cars.findAll");
              return query.getResultList();
      })
                .toMulti()
                .flatMap(l -> Multi.createFrom().items(l.stream().map(car -> new CarDto(car))));
    }

    @GET
    @Path("{id}")
    public Uni<Response> getCar(@PathParam("id") Integer id) {
      LOGGER.info("Getting car " + id);
      return this.client.withTransaction((s,t) ->
              s.find(Car.class, id)
                      .onItem().ifNotNull().transformToUni(car -> Uni.createFrom().item(Response.ok(new CarDto(car)).build()))
                      .onItem().ifNull().continueWith(Response.serverError().status(Status.NOT_FOUND).build())
        );
    }

    @GET
    @Path("types")
    public Uni<Response> getTypes() {
      return Uni.createFrom().item(Response.ok(CarType.values()).build());
    }

    @POST
    public Uni<Response> create(@NotNull CarDto cardto) {
      LOGGER.info("Creating car of type " + cardto.getType() + " with name " + cardto.getName());

      // Needs to be null in order to be persisted
      cardto.setId(null);

      Car car = new Car(cardto);

      return this.client.withTransaction((s,t) -> s.persist(car)).onItem()
        .transformToUni(u -> {
            LOGGER.info(
                "Created car of type " + car.getType() + " with name " + car.getName() + " and id " + car.getId());

            try {
              kafkaService.createTopic(TOPIC_PREFIX + car.getId());
            } catch (TopicCreationFailedException e) {
              LOGGER.error("Couldn't create topic " + e.getTopic());
              return delete(car.getId());
            }

            return Uni.createFrom().item(Response.ok(new CarDto(car)).status(Status.CREATED).build());
        });
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@PathParam("id") Integer id) {
      LOGGER.info("Removing car " + id);

      return this.client.withTransaction((s,t) ->
              s.find(Car.class, id)

                // If entity exists then delete it
                .onItem().ifNotNull()
                .transformToUni(car -> {
                  s.remove(car);

                      try {
                        kafkaService.deleteTopic(TOPIC_PREFIX + car.getId());
                      } catch (TopicDeletionFailedException e) {
                        LOGGER.error("Couldn't delete topic " + e.getTopic());
                      }

                    return Uni.createFrom().item(Response.ok().status(Status.NO_CONTENT).build());
                  })

                // If not found
                .onItem().ifNull()
                .continueWith(Response.ok().status(Status.NOT_FOUND).build()));
    }

    @PUT
    @Path("{id}/{name}/{type}/{price}")
    public Uni<Response> update(@PathParam("id") Integer id , @PathParam("name") @NotNull String name, @PathParam("type") @NotNull CarType type, @PathParam("price") double price) {
      return this.client.withTransaction((s,t) -> s.find(Car.class, id)

          // Update if exists
          .onItem().ifNotNull()
          .invoke(car -> {
            car.setName(name);
            car.setType(type);
            car.setPrice(price);
          })
          .onItem().ifNotNull()
          .transform(car -> Response.ok(new CarDto(car)).build())

          // If not found
          .onItem().ifNull()
          .continueWith(Response.ok().status(Status.NOT_FOUND).build() )
      );
    }
}
