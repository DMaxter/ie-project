package pt.tecnico.avaas.services;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

import io.smallrye.mutiny.Uni;
import pt.tecnico.avaas.dto.CarDto;
import pt.tecnico.avaas.dto.UserDto;
import pt.tecnico.avaas.model.Apilot;
import pt.tecnico.avaas.model.Car;
import pt.tecnico.avaas.model.User;

@Path("/shopping")
@ApplicationScoped
@Consumes("application/json")
@Produces("application/json")
public class BuyService {
  private static final Logger LOGGER = Logger.getLogger(BuyService.class);

  @Inject
  Mutiny.SessionFactory client;

  @GET
  @Path("{id}")
  public Uni<Response> getCar(@PathParam("id") Integer id) {
    LOGGER.info("Getting car from user " + id);

    return this.client.withTransaction((s, t) -> s.find(User.class, id)
        .onItem().ifNotNull().transform(user -> Response.ok(new CarDto(user.getCar())).build())
        .onItem().ifNull().continueWith(() -> Response.status(Status.NOT_FOUND.getStatusCode()).build()));
  }

  @PUT
  @Path("buy/{userid}/{carid}")
  public Uni<Response> buy(@PathParam("userid") Integer userid, @PathParam("carid") Integer carid) {
    LOGGER.info("User " + userid + " is buying car " + carid);

    return this.client.withTransaction((s,t) -> {
      Uni<User> userUni =  s.find(User.class, userid);
      Uni<Car> carUni = s.find(Car.class, carid);

      return Uni.combine().all().unis(userUni, carUni).asTuple().onItem().transform(tuple -> {
        User user = tuple.getItem1();
        Car car = tuple.getItem2();

        if (user == null) {
          LOGGER.error("User " + userid + " doesn't exist");
          return Response.status(Status.NOT_FOUND).build();
        } else if (car == null) {
          LOGGER.error("Car " + carid + " doesn't exist");
          return Response.status(Status.NOT_FOUND).build();
        } else if (car.getUser() != null) {
          LOGGER.error("Car " + carid + " is already owned");
          return Response.status(Status.CONFLICT).build();
        } else if (user.getBalance() < car.getPrice()) {
          LOGGER.error("User " + userid + " doesn't have enough money to buy car " + carid);
          return Response.status(Status.PAYMENT_REQUIRED).build();
        } else {
          LOGGER.info("User " + userid + " successfully bought car " + carid);

          car.setUser(user);
          user.setCar(car);
          user.setBalance(user.getBalance() - car.getPrice());

          return Response.ok(new UserDto(user)).build();
        }
      });
    });
  }

  @PUT
  @Path("sell/{userid}")
  public Uni<Response> sell(@PathParam("userid") Integer userid) {
    LOGGER.info("User " + userid + " is selling their car");

    return this.client.withTransaction((s,t) -> s.find(User.class, userid).onItem().ifNotNull().transform(user -> {
      if (user.getCar() == null) {
        LOGGER.error("User " + userid + " doesn't have a car");
        return Response.status(Status.EXPECTATION_FAILED).build();
      } else {
        LOGGER.info("User " + userid + " sold their car");

        Car car = user.getCar();
        Apilot apilot = user.getApilot();

        if (apilot != null) {
          apilot.setUser(null);
          user.setApilot(null);
        }

        car.setUser(null);
        user.setCar(null);
        user.setBalance(user.getBalance() + car.getPrice());

        return Response.ok(new UserDto(user)).build();
      }
    }));
  }
}
