package pt.tecnico.avaas.services;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

import io.smallrye.common.constraint.NotNull;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import pt.tecnico.avaas.dto.UserDto;
import pt.tecnico.avaas.exceptions.TopicCreationFailedException;
import pt.tecnico.avaas.exceptions.TopicDeletionFailedException;
import pt.tecnico.avaas.model.User;

@Path("/user")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class UserService {
  private static final Logger LOGGER = Logger.getLogger(UserService.class);
  public static final String TOPIC_PREFIX = "user";

  @Inject
  Mutiny.SessionFactory client;

  @Inject
  KafkaService kafkaService;

  @GET
  public Multi<UserDto> getAll() {
    LOGGER.debug("Getting users");

    return this.client.withTransaction((s, t) -> s.createNamedQuery("Users.findAll").getResultList())
        .toMulti()
        .flatMap(l -> Multi.createFrom().items(l.stream().map(e -> new UserDto((User) e))));
  }

  @GET
  @Path("{id}")
  public Uni<Response> getUser(@PathParam("id") Integer id) {
    return this.client.withTransaction((s, t) -> s.find(User.class, id)
        .onItem().ifNotNull().transformToUni(user -> Uni.createFrom().item(Response.ok(new UserDto(user)).build())))
        .onItem().ifNull().continueWith(Response.serverError().status(Status.NOT_FOUND).build());
  }

  @POST
  public Uni<Response> create(@NotNull UserDto userdto) {
    LOGGER.info("Creating user with name " + userdto.getName());

    // Needs to be null in order to be persisted
    userdto.setId(null);

    User user = new User(userdto);

    return this.client.withTransaction((s, t) -> s.persist(user)).onItem().transformToUni(u -> {
      LOGGER.info("Created user with id " + user.getId() + " and name " + user.getName());

      try {
        kafkaService.createTopic(TOPIC_PREFIX + user.getId());
      } catch (TopicCreationFailedException e) {
        LOGGER.error("Couldn't create topic " + e.getTopic());
        return delete(user.getId());
      }

      return Uni.createFrom().item(Response.ok(new UserDto(user)).status(Status.CREATED).build());
    });
  }

  @DELETE
  @Path("{id}")
  public Uni<Response> delete(@PathParam("id") Integer id) {
    LOGGER.info("Removing user " + id);

    return this.client.withTransaction((s, t) -> s.find(User.class, id)

        // If entity exists then delete it
        .onItem().ifNotNull()
        .transformToUni(user -> {
          s.remove(user);

          try {
            kafkaService.deleteTopic(TOPIC_PREFIX + user.getId());
          } catch (TopicDeletionFailedException e) {
            LOGGER.error("Couldn't delete topic " + e.getTopic());
          }

          return Uni.createFrom().item(Response.ok().status(Status.NO_CONTENT).build());
        })

        // If not found
        .onItem().ifNull()
        .continueWith(Response.ok().status(Status.NOT_FOUND).build()));
  }

  @PUT
  @Path("/{id}/{iq}/{eq}/{aq}/{balance}")
  public Uni<Response> update(@PathParam("id") Integer id, @PathParam("iq") boolean iq, @PathParam("eq") boolean eq,
      @PathParam("aq") boolean aq, @PathParam("balance") double balance) {
    LOGGER.debug("Updating user");

    return this.client.withTransaction((s, t) -> s.find(User.class, id)

        // Update if exists
        .onItem().ifNotNull()
        .invoke(user -> {
          user.setAq(aq);
          user.setEq(eq);
          user.setIq(iq);
          user.setBalance(balance);
        })
        .onItem().ifNotNull()
        .transform(user -> Response.ok(new UserDto(user)).build())

        // If not found
        .onItem().ifNull()
        .continueWith(Response.ok().status(Status.NOT_FOUND).build()));
  }

  @POST
  @Path("/notify/{id}")
  public Uni<Response> notify(@PathParam("id") Integer id, String actions) {
    LOGGER.info("Received actions for user " + id);

    return kafkaService.sendToUserTopic(TOPIC_PREFIX + id, actions).onFailure().recoverWithItem(Response.serverError().build()).onItem().ifNotNull().transform(i -> Response.ok().build()).onItem().ifNull().continueWith(Response.serverError().build());
  }
}
