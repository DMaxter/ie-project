package pt.tecnico.avaas.services;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;

import io.smallrye.common.constraint.NotNull;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import pt.tecnico.avaas.dto.ApilotDto;
import pt.tecnico.avaas.dto.UserDto;
import pt.tecnico.avaas.exceptions.TopicCreationFailedException;
import pt.tecnico.avaas.exceptions.TopicDeletionFailedException;
import pt.tecnico.avaas.model.Apilot;
import pt.tecnico.avaas.model.User;


@Path("/apilot")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class ApilotService {
    private static final Logger LOGGER = Logger.getLogger(ApilotService.class);
    public static final String TOPIC_PREFIX = "apilot";

    @Inject
    Mutiny.SessionFactory client;

    @Inject
    KafkaService kafkaService;

    @GET
    public Multi<ApilotDto> getAll() {
        LOGGER.debug("Getting pilots");

        return this.client.withTransaction((s,t) -> s.createNamedQuery("Apilots.findAll").getResultList())
                .toMulti()
                .flatMap(l -> Multi.createFrom().items(l.stream().map(e -> new ApilotDto((Apilot) e))));
    }

    @GET
    @Path("{id}")
    public Uni<Response> getApilot(@PathParam("id") Integer id) {
      return this.client.withTransaction((s,t) ->
              s.find(Apilot.class, id)
                      .onItem().ifNotNull().transformToUni(pilot -> Uni.createFrom().item(Response.ok(new ApilotDto(pilot)).build()))
                      .onItem().ifNull().continueWith(Response.status(Status.NOT_FOUND).build())
        );
    }

    @POST
    public Uni<Response> create(@NotNull ApilotDto pilotdto) {
      LOGGER.info("Creating apilot " + pilotdto.getName() + " and price " + pilotdto.getPrice());

        // Needs to be null in order to be persisted
        pilotdto.setId(null);

        Apilot pilot = new Apilot(pilotdto);

        return this.client.withTransaction((s,t) -> s.persist(pilot))
                .onItem().transformToUni(u -> {
              LOGGER.info(
                "Created apilot " + pilot.getName() + " and id " + pilot.getId());

            try {
              kafkaService.createTopic(TOPIC_PREFIX + pilot.getId());
            } catch (TopicCreationFailedException e) {
              LOGGER.error("Couldn't create topic " + e.getTopic());
              return delete(pilot.getId());
            }

              return Uni.createFrom().item(Response.ok(new ApilotDto(pilot)).status(Status.CREATED).build());
            });
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@PathParam("id") Integer id) {
        LOGGER.debug("Deleting pilot " + id);

        return this.client.withTransaction((s,t) ->
                s.find(Apilot.class, id)

                        // If entity exists then delete it
                        .onItem().ifNotNull()
                    .transformToUni(pilot -> {
                      s.remove(pilot);

                      try {
                        kafkaService.deleteTopic(TOPIC_PREFIX + pilot.getId());
                      } catch (TopicDeletionFailedException e) {
                        LOGGER.error("Couldn't delete topic " + e.getTopic());
                      }

                      return Uni.createFrom().item(Response.status(Status.NO_CONTENT).build());
                    })

                        // If not found
                        .onItem().ifNull()
                        .continueWith(Response.status(Status.NOT_FOUND).build()));
    }

    @PUT
    @Path("{id}/{name}/{price}")
    public Uni<Response> update(@PathParam("id") Integer id , @PathParam("name") @NotNull String name, @PathParam("price") double price) {
        LOGGER.debug("Updating apilot");

        return this.client.withTransaction((s,t) -> s.find(Apilot.class, id)

                // Update if exists
                .onItem().ifNotNull()
                .invoke(pilot -> {
                    pilot.setName(name);
                    pilot.setPrice(price);
                })
                .onItem().ifNotNull()
                .transform(pilot -> Response.ok(new ApilotDto(pilot)).build())

                // If not found
                .onItem().ifNull()
                .continueWith(Response.status(Status.NOT_FOUND).build() )
        );
    }
    @PUT
    @Path("selection/{userid}/{apilotid}")
    public Uni<Response> selectApilot(@PathParam("userid") Integer userid , @PathParam("apilotid")  Integer apilotid ) {
        LOGGER.info("User " + userid + " is selecting pilot " + apilotid);

        return this.client.withTransaction((s,t) -> {
          Uni<User> userUni = s.find(User.class, userid);
          Uni<Apilot> apilotUni = s.find(Apilot.class, apilotid);

          return Uni.combine().all().unis(userUni, apilotUni).asTuple().onItem().transform(tuple -> {
            User user = tuple.getItem1();
            Apilot pilot = tuple.getItem2();

            if (user == null) {
              LOGGER.error("User doesn't exist");
              return Response.status(Status.NOT_FOUND).build();
            } else if (user.getCar() == null) {
                LOGGER.error("User doesn't have any car");
                return Response.status(Status.PRECONDITION_FAILED).build();
            } else if (pilot == null) {
              LOGGER.error("APILOT doesn't exist");
              return Response.status(Status.NOT_FOUND).build();
            } else if (user.getBalance() < pilot.getPrice()) {
              LOGGER.error("User " + userid + " doesn't have enough money to select pilot " + apilotid);
              return Response.status(Status.PAYMENT_REQUIRED).build();
            } else {
              LOGGER.info("User " + userid + " successfully selected pilot " + apilotid);
              user.setApilot(pilot);
              pilot.setUser(user);
              user.setBalance(user.getBalance() - pilot.getPrice());
              return Response.ok(new UserDto(user)).build();
            }
          });
        });
    }

    @PUT
    @Path("selection/{userid}")
    public Uni<Response> unselect(@PathParam("userid") Integer userid) {
      LOGGER.info("User " + userid + " is removing their pilot");

      return this.client.withTransaction((s,t) -> {
        return s.find(User.class, userid).onItem().ifNotNull().transform(user -> {
          if (user.getApilot() == null) {
              LOGGER.error("User " + userid + " doesn't have a pilot for car");
              return Response.status(Status.EXPECTATION_FAILED).build();
          } else if (user.getCar() == null) {
              LOGGER.error("User " + userid + " doesn't have a car");
              return Response.status(Status.NOT_FOUND).build();
          } else {
            LOGGER.info("User " + userid + " removed their pilot");
            Apilot pilot = user.getApilot();
            if (pilot != null) {
              pilot.setUser(null);
            }

            user.setApilot(null);
            return Response.ok(new UserDto(user)).build();
          }
        });
      });
    }
}
