import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.*;

import io.quarkus.test.junit.QuarkusTest;
import pt.tecnico.avaas.dto.CarDto;
import pt.tecnico.avaas.dto.UserDto;
import pt.tecnico.avaas.model.*;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import javax.ws.rs.core.Response.Status;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BuyTests {
    public static final String USER_NAME = "Mia Phillips";
    public static final Boolean  AQ = Boolean.TRUE;
    public static final Boolean  EQ = Boolean.FALSE;
    public static final Boolean  IQ = Boolean.TRUE;
    public static final double USER_BALANCE = 800.5;
    public static final String CAR_NAME = "Mater the truck";
    public static final CarType CAR_TYPE = CarType.TRUCK;
    public static final double CAR_PRICE = 500.3;

    public static UserDto user = null;
    public static CarDto car = null;

    public static Integer associationID = null;

    @BeforeAll
    public static void setup() {
        RestAssured.defaultParser = Parser.JSON;

        user = new UserDto();
        user.setEq(EQ);
        user.setAq(AQ);
        user.setIq(IQ);
        user.setName(USER_NAME);
        user.setBalance(USER_BALANCE);

        user = given().port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class)).when()
                .body(user).contentType(ContentType.JSON).post("/user").then()
                .extract().response().getBody().as(UserDto.class);

        car = new CarDto();
        car.setType(CAR_TYPE);
        car.setName(CAR_NAME);
        car.setPrice(CAR_PRICE);

        car = given().port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class)).when()
                .body(car).contentType(ContentType.JSON).post("/car").then()
                .extract().response().getBody().as(CarDto.class);
    }

    @Test
    @Order(1)
    public void buyCar() {
      UserDto userResponse = given().when()
          .put("/shopping/buy/" + user.getId() + "/" + car.getId()).then()
          .contentType(ContentType.JSON).extract().response()
          .getBody().as(UserDto.class);

        assert userResponse.getCar() != null;
        assertEquals(userResponse.getCar(), car);
        assertNull(userResponse.getApilot());

        user = userResponse;
    }

    @Test
    @Order(2)
    public void sellCar() {
      UserDto userResponse = given().when()
          .put("/shopping/sell/" + user.getId()).then()
          .contentType(ContentType.JSON).extract().response()
          .getBody().as(UserDto.class);

        assert userResponse.getCar() == null;

        cleanup();
    }

    public void cleanup() {
      given().delete("/user/" + user.getId()).then().statusCode(Status.NO_CONTENT.getStatusCode());
    }
}
