import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import pt.tecnico.avaas.dto.UserDto;
import pt.tecnico.avaas.services.UserService;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;


@QuarkusTest
@TestHTTPEndpoint(UserService.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserTests {
  public static final String USERNAME = "Mia Phillips";
  public static final Boolean AQ = true;
  public static final Boolean EQ = false;
  public static final Boolean IQ = true;
  public static Integer id = null;
  public static final double BALANCE = 400.4;
  public static final double BALANCE2 = 1337;

  @Test
  @Order(1)
  public void createUser() {
    UserDto user = new UserDto();
    user.setName(USERNAME);
    user.setAq(AQ);
    user.setEq(EQ);
    user.setIq(IQ);
    user.setBalance(BALANCE);

    Response response = given()
        .body(user).contentType(ContentType.JSON).post().then()
        .statusCode(Status.CREATED.getStatusCode()).contentType(ContentType.JSON)
        .extract().response();

    UserDto userResponse = response.getBody().as(UserDto.class);

    assertNotNull(userResponse);
    assertNotNull(userResponse.getId());
    assertEquals(userResponse.getAq(), AQ);
    assertEquals(userResponse.getEq(), EQ);
    assertEquals(userResponse.getIq(), IQ);
    assertEquals(userResponse.getName(), USERNAME);
    assertEquals(userResponse.getBalance(), BALANCE);

    id = userResponse.getId();
  }

  @Test
  @Order(2)
  public void getUser() {
    assert id != null;

    Response response = given()
        .get("" + id).then()
        .statusCode(Status.OK.getStatusCode()).contentType(ContentType.JSON)
        .extract().response();

    UserDto user = response.getBody().as(UserDto.class);

    assertNotNull(user);
    assertNotNull(user.getId());
    assertEquals(user.getAq(), AQ);
    assertEquals(user.getEq(), EQ);
    assertEquals(user.getIq(), IQ);
    assertEquals(user.getName(), USERNAME);
  }

  @Test
  @Order(3)
  public void updateUser() {
    assert id != null;

    Response response = given()
        .put(id + "/" + !IQ + "/" + !EQ + "/" + !AQ + "/" + BALANCE2).then()
        .statusCode(Status.OK.getStatusCode()).contentType(ContentType.JSON)
        .extract().response();

    UserDto user = response.getBody().as(UserDto.class);

    assertNotNull(user);
    assertNotNull(user.getId());
    assertEquals(user.getAq(), !AQ);
    assertEquals(user.getEq(), !EQ);
    assertEquals(user.getIq(), !IQ);
    assertEquals(user.getName(), USERNAME);
    assertEquals(user.getBalance(), BALANCE2);
  }

  @Test
  @Order(4)
  public void deleteUser() {
    assert id != null;

    given()
      .delete("" + id).then()
        .statusCode(Status.NO_CONTENT.getStatusCode());

    given()
      .get("" + id).then()
        .statusCode(Status.NOT_FOUND.getStatusCode());
  }
}