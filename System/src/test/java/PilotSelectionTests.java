import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.eclipse.microprofile.config.ConfigProvider;
import org.junit.jupiter.api.*;

import pt.tecnico.avaas.dto.ApilotDto;
import pt.tecnico.avaas.dto.CarDto;
import pt.tecnico.avaas.dto.UserDto;
import pt.tecnico.avaas.model.*;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import javax.ws.rs.core.Response.Status;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PilotSelectionTests {
    public static final String APILOT_NAME = "meow";
    public static final String USER_NAME = "Mia Phillips";
    public static final Boolean  AQ = true;
    public static final Boolean  EQ = false;
    public static final Boolean  IQ = true;
    public static final String CAR_NAME = "Mater the truck";
    public static final CarType CAR_TYPE = CarType.TRUCK;
    public static final double APILOT_PRICE = 400;
    public static final double CAR_PRICE = 200;
    public static final double USER_BALANCE = 1000;

    public static UserDto user = null;
    public static ApilotDto apilot = null;
    public static CarDto car = null;

    @BeforeAll
    public static void setup() {
        RestAssured.defaultParser = Parser.JSON;

        user = new UserDto();
        user.setEq(EQ);
        user.setAq(AQ);
        user.setIq(IQ);
        user.setName(USER_NAME);
        user.setBalance(USER_BALANCE);

        user = given().port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class)).when()
                .body(user).contentType(ContentType.JSON).post("/user").then()
                .extract().response().getBody().as(UserDto.class);

        car = new CarDto();
        car.setType(CAR_TYPE);
        car.setName(CAR_NAME);
        car.setPrice(CAR_PRICE);

        car = given().port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class)).when()
                .body(car).contentType(ContentType.JSON).post("/car").then()
                .extract().response().getBody().as(CarDto.class);

        apilot = new ApilotDto();
        apilot.setName(APILOT_NAME);
        apilot.setPrice(APILOT_PRICE);

        apilot = given().port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class)).when()
                .body(apilot).contentType(ContentType.JSON).post("/apilot").then()
                .extract().response().getBody().as(ApilotDto.class);

        user = given().port(ConfigProvider.getConfig().getValue("quarkus.http.test-port", Integer.class)).when()
                .put("/shopping/buy/" + user.getId() + "/" + car.getId()).then()
              .extract().response().getBody().as(UserDto.class);
    }

    @Test
    @Order(1)
    public void selectPilot() {
        UserDto userResponse = given().when().put("/apilot/selection/" + user.getId() + "/" + apilot.getId()).then().contentType(ContentType.JSON).extract().response().getBody().as(UserDto.class);

        assert userResponse.getCar() != null;
        assertEquals(userResponse.getCar(), car);
        assertEquals(userResponse.getApilot(), apilot);

        user = userResponse;
    }

    @Test
    @Order(2)
    public void unselectPilot() {
        UserDto userResponse = given().when().put("/apilot/selection/" + user.getId()).then().contentType(ContentType.JSON).extract().response().getBody().as(UserDto.class);

        assert userResponse.getCar() != null;
        assertEquals(userResponse.getCar(), car);
        assertNull(userResponse.getApilot());

        user = userResponse;

        cleanup();
    }

    public void cleanup() {
      given().delete("/user/" + user.getId()).then().statusCode(Status.NO_CONTENT.getStatusCode());
      given().delete("/apilot/" + apilot.getId()).then().statusCode(Status.NO_CONTENT.getStatusCode());
    }
}
