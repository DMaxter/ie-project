import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import pt.tecnico.avaas.dto.ApilotDto;
import pt.tecnico.avaas.services.ApilotService;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;


@QuarkusTest
@TestHTTPEndpoint(ApilotService.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApilotTests {
    public static final String APILOTNAME = "Mia Phillips";
    public static final String APILOTNAME2 = "Maria Filipe";
    public static final double PRICE = 5.6;
    public static final double PRICE2 = 10.0;
    public static Integer id = null;

    @Test
    @Order(1)
    public void createApilot() {
        ApilotDto apilot = new ApilotDto();
        apilot.setName(APILOTNAME);
        apilot.setPrice(PRICE);


        Response response = given()
                .body(apilot).contentType(ContentType.JSON).post().then()
                .statusCode(Status.CREATED.getStatusCode()).contentType(ContentType.JSON)
                .extract().response();

        ApilotDto apilotResponse = response.getBody().as(ApilotDto.class);

        assert apilotResponse != null;
        assert apilotResponse.getId() != null;
        id = apilotResponse.getId();
        assertEquals(apilotResponse.getName(), APILOTNAME);
        assertEquals(apilotResponse.getPrice(), PRICE);
    }

    @Test
    @Order(2)
    public void getApilot() {
        assert id != null;

        Response response = given()
                .get("" + id).then()
                .statusCode(Status.OK.getStatusCode()).contentType(ContentType.JSON)
                .extract().response();

        ApilotDto apilot = response.getBody().as(ApilotDto.class);

        assert apilot != null;
        assert apilot.getId() != null;
        assertEquals(apilot.getName(), APILOTNAME);
        assertEquals(apilot.getPrice(), PRICE);
    }

    @Test
    @Order(3)
    public void updateApilot() {
        assert id != null;

        Response response = given()
                .put(id + "/" + APILOTNAME2 + "/" + PRICE2).then()
                .statusCode(Status.OK.getStatusCode()).contentType(ContentType.JSON)
                .extract().response();

        ApilotDto apilot = response.getBody().as(ApilotDto.class);

        assert apilot != null;
        assert apilot.getId() != null;
        assertEquals(apilot.getName(), APILOTNAME2);
        assertEquals(apilot.getPrice(), PRICE2);
    }

    @Test
    @Order(4)
    public void deleteApilot() {
        assert id != null;

        given()
                .delete("" + id).then()
                .statusCode(Status.NO_CONTENT.getStatusCode());

        given()
                .get("" + id).then()
                .statusCode(Status.NOT_FOUND.getStatusCode());
    }
}