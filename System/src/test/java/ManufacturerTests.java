import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import pt.tecnico.avaas.dto.CarDto;
import pt.tecnico.avaas.model.CarType;
import pt.tecnico.avaas.services.ManufacturerService;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;


@QuarkusTest
@TestHTTPEndpoint(ManufacturerService.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ManufacturerTests {
    public static final String CARNAME = "lightning Mcqueen";
    public static final String CARNAME2 = "kachaw";
    public static final CarType TYPE = CarType.SUV;
    public static final CarType TYPE2 = CarType.BUS;
    public static final double PRICE = 1200.0;
    public static final double PRICE2 = 2037.2;

    public static Integer id = null;

    @Test
    @Order(1)
    public void createCar() {
        CarDto car = new CarDto();
        car.setName(CARNAME);
        car.setType(TYPE);
        car.setPrice(PRICE);


        Response response = given()
                .body(car).contentType(ContentType.JSON).post().then()
                .statusCode(Status.CREATED.getStatusCode()).contentType(ContentType.JSON)
                .extract().response();

        CarDto carResponse = response.getBody().as(CarDto.class);

        assert carResponse != null;
        assert carResponse.getId() != null;
        id = carResponse.getId();
        assertEquals(carResponse.getName(), CARNAME);
        assertEquals(carResponse.getType(), TYPE);
        assertEquals(carResponse.getPrice(), PRICE);
    }

    @Test
    @Order(2)
    public void getCar() {
        assert id != null;

        Response response = given()
                .get("" + id).then()
                .statusCode(Status.OK.getStatusCode()).contentType(ContentType.JSON)
                .extract().response();

        CarDto car = response.getBody().as(CarDto.class);

        assert car != null;
        assert car.getId() != null;
        assertEquals(car.getName(), CARNAME);
        assertEquals(car.getType(), TYPE);
        assertEquals(car.getPrice(), PRICE);
    }

    @Test
    @Order(3)
    public void updateCar() {
        assert id != null;

        Response response = given()
                .put(id + "/" + CARNAME2 + "/" + TYPE2 + "/" + PRICE2).then()
                .statusCode(Status.OK.getStatusCode()).contentType(ContentType.JSON)
                .extract().response();

        CarDto car = response.getBody().as(CarDto.class);

        assert car != null;
        assert car.getId() != null;
        assertEquals(car.getName(), CARNAME2);
        assertEquals(car.getType(), TYPE2);
        assertEquals(car.getPrice(), PRICE2);
    }

    @Test
    @Order(4)
    public void deleteCar() {
        assert id != null;

        given()
                .delete("" + id).then()
                .statusCode(Status.NO_CONTENT.getStatusCode());

        given()
                .get("" + id).then()
                .statusCode(Status.NOT_FOUND.getStatusCode());
    }
}