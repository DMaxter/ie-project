package pt.tecnico.avaas;

import java.util.Properties;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.List;
import java.sql.Timestamp;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class AVaaSMessageProvider {

  static String brokerList = "localhost:9092";
  static int throughput = 10;
  static String filterprefix = "car";

  static List<String> topics;

  private static String getRandomTopic() {
    int index = (new Random()).nextInt(topics.size());
    return topics.get(index);
  }

  private static Message CreateMessage(String topicToSend, Timestamp ts) {
    Message newMessage;
    newMessage = new Message();
    newMessage.setTimeStamp(ts.toString());
    newMessage.setSeqkey(topicToSend + "_" + String.valueOf(((Double) (Math.random() * 10)).intValue()));
    newMessage.setAV_ID(topicToSend);

    newMessage.setSpeed(String.valueOf(((Double) (Math.random() * 120)).intValue()));
    newMessage.setBatteryLevel(String.valueOf(((Double) (Math.random() * 100)).intValue()));
    newMessage.setDriverTirenessLevel(String.valueOf(((Double) (Math.random() * 100)).intValue()));
    newMessage.setTractionWheelsLevel(String.valueOf(((Double) (Math.random() * 100)).intValue()));

    String[] locationOptions = { "38.73704375907657, -9.138709213484344", "38.73730834347317, -9.302641438338373",
        "38.81178965928624, -9.093170965989835", "Unknown" };
    newMessage.setLocation(locationOptions[new Random().nextInt(locationOptions.length)]);

    String[] EnvironmentalLightningOptions = { "N/A", "Bad", "Sufficient", "Good", "Very Good", "Excelent" };
    newMessage.setEnvironmentalLightning(
        EnvironmentalLightningOptions[new Random().nextInt(EnvironmentalLightningOptions.length)]);
    String[] RainConditionsOptions = { "N/A", "Light Rain", "Medium Rain", "Heavy Rain" };
    newMessage.setRainConditions(RainConditionsOptions[new Random().nextInt(RainConditionsOptions.length)]);
    String[] FogConditionsOptions = { "N/A", "None", "Light Fog", "Medium Fog", "Dense Fog" };
    newMessage.setFogConditions(FogConditionsOptions[new Random().nextInt(FogConditionsOptions.length)]);

    newMessage.setAsText(
        "{\"AV_Event\":{" +
            "\"TimeStamp\":\"" + newMessage.getTimeStamp() + "\"," +
            "\"AV_ID\":\"" + newMessage.getAV_ID() + "\"," +
            "\"Speed\":\"" + newMessage.getSpeed() + "\"," +
            "\"BatteryLevel\":\"" + newMessage.getBatteryLevel() + "\"," +
            "\"DriverTirenessLevel\":\"" + newMessage.getDriverTirenessLevel() + "\"," +
            "\"Location\":\"" + newMessage.getLocation() + "\"," +
            "\"EnvironmentalLightning\":\"" + newMessage.getEnvironmentalLightning() + "\"," +
            "\"RainConditions\":\"" + newMessage.getRainConditions() + "\"," +
            "\"FogConditions\":\"" + newMessage.getFogConditions() + "\"," +
            "\"TractionWheelsLevel\":\"" + newMessage.getTractionWheelsLevel() + "\"" +
            "}}");

    return newMessage;
  }

  private static void printArgs() {
    System.out.println(
        "--broker-list=" + brokerList + "\n" +
            "--throughput=" + throughput);
  }

  private static boolean checkArgs(String[] args) {
    for (int i = 0; i < args.length; i = i + 2) {
      if (args[i].compareTo("--broker-list") == 0) {
        brokerList = args[i + 1];
      } else if (args[i].compareTo("--throughput") == 0) {
        throughput = Integer.valueOf(args[i + 1]).intValue();
      } else {
        System.out.println("Bad argument: " + args[i]);
        return false;
      }
    }

    if (brokerList.length() == 0) {
      System.out.println("Broker-list argument is mandatory!");
    } else {
      return true;
    }

    return false;
  }

  private static void SendMessage(Message msg, KafkaProducer<String, String> prd, String topicTarget) {
    System.out.println("This is the message to send = " + msg.getAsText());
    String seqkey = msg.getSeqkey();

    System.out.println("Sending new message to Kafka, to the topic = " + topicTarget + ", with key=" + seqkey);

    ProducerRecord<String, String> record = new ProducerRecord<>(topicTarget, seqkey, msg.getAsText());

    prd.send(record);

    System.out.println("Sent...");
  }

  public static void main(String[] args) {
    String usage = "\nThe usage of the Message Producer for AVaaS is the following.\n\n"
        + "AVaaSSimulator "
        + "--broker-list <brokers> "
        + "--throughput <value> "
        + "\n"
        + "where, \n"
        + "--broker-list: is a broker list with ports (e.g.: kafka02.example.com:9092,kafka03.example.com:9092), default value is localhost:9092\n"
        + "--throughput: is the approximate maximum messages to be produced by minute, default value is 10\n";

    Properties kafkaProps = new Properties();

    if (args.length == 0) {
      System.out.println(usage);
    } else {
      if (checkArgs(args)) {
        System.out.println("The following arguments are accepted:");
        printArgs();
        System.out.println("------- Processing starting -------");

        kafkaProps.put("bootstrap.servers", brokerList);
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(kafkaProps);

        // Check topics
        Properties props = new Properties();
        props.put("bootstrap.servers", brokerList);
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        topics = consumer.listTopics().keySet().stream().filter(topic -> topic.startsWith(filterprefix))
            .collect(Collectors.toList());
        consumer.close();

        System.out.println("Topics discovered: ");
        for (String topicName : topics) {
          System.out.println("Topic = " + topicName);
        }

        while (true) {
          try {
            Timestamp mili = new Timestamp(System.currentTimeMillis());

            if (!topics.isEmpty()) {
              String topic_to_send = getRandomTopic();

              Message messageToSend = CreateMessage(topic_to_send, mili);

              if (messageToSend != null) {
                SendMessage(messageToSend, producer, topic_to_send);
              }
            } else {
              System.out.println("Empty list of Topics. Therefore, no message to send.");
              break;
            }

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println("Waiting..." + timestamp);
            Thread.sleep(60000 / throughput);
          } catch (Exception e) {
            e.printStackTrace();
          }

          System.out.println("Fire-and-forget stopped");
        }

        producer.close();
      } else {
        System.out.println("Application Arguments bad usage.\n\nPlease check syntax.\n\n" + usage);
      }
    }
  }

}
